package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends Duck {
    
    public MallardDuck() { // TODO Complete me!
        setFlyBehavior(new FlyWithWings());
        setQuackBehavior(new Quack());
    }

    @Override
    public void display() {
        System.out.println("Mallard Duck");
    }
}