package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck {

    public ModelDuck() {    // TODO Complete me!
        setFlyBehavior(new FlyNoWay());
        setQuackBehavior(new MuteQuack());
    }

    @Override
    public void display() {
        System.out.println("Model Duck");
    }
}