package id.ac.ui.cs.advprog.tutorial1.strategy;

public class FlyNoWay implements FlyBehavior {
    
    public void fly() {    // TODO Complete me!
        System.out.println("Can't fly");
    }
}
