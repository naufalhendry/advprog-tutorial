import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

class Customer {

    private String name;
    private Vector rentalV = new Vector();
    private List<Rental> rentals = new ArrayList<>();

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental arg) {
        rentalV.addElement(arg);
        rentals.add(arg);
    }

    public String getName() {
        return name;
    }

    public String statement() {

        Iterator<Rental> iterator = rentals.iterator();
        String result = "Rental Record for " + getName() + "\n";

        while (iterator.hasNext()) {
            Rental each = iterator.next();

            // Show figures for this rental
            result += "\t" + each.getMovie().getTitle() + "\t" 
                    + String.valueOf(amountFor(each)) + "\n";
        }

        // Add footer lines
        result += "Amount owed is " + String.valueOf(getTotalCharge()) + "\n";
        result += "You earned " + String.valueOf(getTotalFrequentRenterPoints()) 
                + " frequent renter points";

        return result;
    }

    private int getTotalFrequentRenterPoints() {
        int result = 0;
        Enumeration rentals = rentalV.elements();
        while (rentals.hasMoreElements()) {
            Rental each = (Rental) rentals.nextElement();
            result += each.getfrequentRenterPoints();
        }
        
        return result;
    }
    
    private double getTotalCharge() {
        double result = 0;
        Enumeration rentals = rentalV.elements();
        while (rentals.hasMoreElements()) {
            Rental each = (Rental) rentals.nextElement();
            result += each.getCharge();
        }
        
        return result;
    }
    
    private double amountFor(Rental rental) {
        return rental.getCharge();
    }
}