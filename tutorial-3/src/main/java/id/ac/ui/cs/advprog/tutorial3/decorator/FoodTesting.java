package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.CrustySandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThickBunBurger;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThinBunBurger;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.BeefMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cheese;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Lettuce;

public class FoodTesting {

    public static void main (String[] args){
        
        Food foods = new CrustySandwich();
        
        System.out.println(foods.getDescription() + " $" + foods.cost());
        
        Food foods2 = new ThickBunBurger();
        foods2 = new Cheese(foods2);
        foods2 = new BeefMeat(foods2);
        foods2 = new Lettuce(foods2);
        
        System.out.println(foods2.getDescription() + " $" + foods2.cost());
        
        Food foods3 = new ThinBunBurger();
        foods3 = new BeefMeat(foods3);
        foods3 = new BeefMeat(foods3);
        foods3 = new BeefMeat(foods3);
        
        System.out.println(foods3.getDescription() + " $" + foods3.cost());
        
    }
}
